//
//  JaybirdSleepTests.swift
//  JaybirdSleepTests
//
//  Created by Alex Volovoy on 12/2/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import XCTest
@testable import JaybirdSleep

class JaybirdSleepTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExampleData() {
        
        let validDuration = NSTimeInterval(24790)
        let validStartDate = dateFromEpochString("2014-07-31 23:11:37")
        let validEndDate =  dateFromEpochString("2014-08-01 06:31:29")
        let mainBundle = NSBundle.mainBundle()
        if let fileURL = mainBundle.URLForResource("exampleData", withExtension: "csv") {
            do {
                let content = try String(contentsOfURL: fileURL, encoding: NSUTF8StringEncoding)
                if let period = try analyseDataString(content) {
                    XCTAssertEqual(validDuration, period.duration, "Duration is invalid")
                    XCTAssertEqual(validStartDate, period.startTime, "Period Start Date is invalid")
                    XCTAssertEqual(validEndDate, period.endTime, "Period End Date is invalid")
                    
                } else {
                    XCTFail("Period calculated as nil")
                    
                }
            }
            catch let error as DataError {
                XCTFail(error.rawValue)
            }
            catch {
                XCTFail("Fail to load data")
            }
  
        } else {
            XCTFail("File not found")
        }
    }
    
}
