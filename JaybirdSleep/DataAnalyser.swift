//
//  DataAnalyser.swift
//  JaybirdSleep
//
//  Created by Alex Volovoy on 12/2/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import Foundation

// Activity we are looking for. Per pdf it looks like its for measuring sleep
let sleepActivityId = 8
// Number of epoch to calculate start
let rollingPercentCount = 150
/// Number of epoch to calculate start
let consecutiveEpochsToStartPeriod = 150
// Number of epoch to calculate end of the period
let consecutiveEpochsToEndPeriod = 375
// Threshhold
let threshold = 0.96
// Let us assume each EPOCH = 10 seconds ( per pdf )
let secPerEpoch = 10

// Start Trigger
let startTrigger = Double(rollingPercentCount) * threshold
// Date formaters

let dateFormatter: NSDateFormatter = NSDateFormatter()

enum DataError : String, ErrorType {
    case STARTNOTFOUND = "Start not found"
    case ENDNOTFOUND = "End not found"
    case DATAINVALID = "Data appears to be invalid"
    case NETWORKERROR = "Couldn't download remote file"

}

func analyseDataString (dataString : String?) throws  -> Period? {
    var startRollingIndex = 0
    var startConsecutiveIndex = -1
    var numberOfEpochWithRollingPercent = 0
    
    if let dataArray = dataString?.componentsSeparatedByString("\n") {
        var period = Period ()
        var epochArray =  [Epoch]()
        epochArray.reserveCapacity(dataArray.count)
        var consecutiveEpochs = 0
        var startPeriodIndex = 0
        for epochString in dataArray {
            if let newEpoch = Epoch(epochString: epochString) {
                epochArray.append(newEpoch)
                if newEpoch.activity_id == sleepActivityId {
                    numberOfEpochWithRollingPercent++
                }
                
                if epochArray.count > rollingPercentCount {
                    let epochToShift = epochArray[startRollingIndex]
                    if epochToShift.activity_id  == sleepActivityId {
                        numberOfEpochWithRollingPercent--
                    }
                    startRollingIndex++
                }
                var  consecutiveEpochsMatched = rollingPercentMatched(numberOfEpochWithRollingPercent)
                if  let _ = period.startTime {
                    // reverse condition for finding end of the period
                    consecutiveEpochsMatched = !consecutiveEpochsMatched
                }
                
                if consecutiveEpochsMatched {
                    consecutiveEpochs++
                    if consecutiveEpochs == 1 {
                        startConsecutiveIndex = epochArray.count - 1
                        //print("First Epoch : \(newEpoch) at index \(startConsecutiveIndex)" )
                    }
                } else {
                    consecutiveEpochs = 0
                }
                if period.startTime == nil {
                    if consecutiveEpochs >= consecutiveEpochsToStartPeriod {
                        // Found START period epoch
                        startPeriodIndex = startConsecutiveIndex - consecutiveEpochsToStartPeriod + 1
                        print("Start Index : \(startPeriodIndex) : \(startConsecutiveIndex)")
                        print("Epoch : \(epochArray [startPeriodIndex])")
                        print("Epoch : \(epochArray [startConsecutiveIndex])")
                        period.startTime = dateFromEpochString(epochArray [startPeriodIndex].timestamp)
                        consecutiveEpochs = 0
                    }
                    
                } else {
                    // Once we found start time - look for the end
                    if consecutiveEpochs >= consecutiveEpochsToEndPeriod {
                        // Found END period epoch
                        // Remove not needed data from array
                        epochArray.removeRange(startConsecutiveIndex...epochArray.count - 1)
                        epochArray.removeRange(0..<startPeriodIndex)
                        // Set end timestamp
                        if let lastEpoch = epochArray.last {
                            period.endTime = dateFromEpochString(lastEpoch.timestamp)
                            print("End Index : \(startConsecutiveIndex)")
                            print("Epoch : \(lastEpoch)")
                        }
                        // Not interested in rest of data
                        break;
                    }
                    
                }
                
            } else {
                print("Failed to import data for row \(index) with string [\(epochString)]")
            }
        }
        guard let _ = period.startTime else { throw DataError.STARTNOTFOUND }
        guard let _ = period.endTime else { throw DataError.ENDNOTFOUND }
        // Considering that each EPOCH is 10 sec - get number of qualified EPOCHs
        let duration = epochArray.filter({ $0.activity_id == 8 }).count * secPerEpoch
        print("Period duration \(Double(duration) / 3600 )")
        period.duration = NSTimeInterval(duration)
        return period
    } else {
         throw DataError.DATAINVALID
    }
}

func dateFromEpochString(dateString: NSString) -> NSDate? {
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let locale = NSLocale (localeIdentifier: "en_US_POSIX")
    dateFormatter.locale = locale
    dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
    
    return dateFormatter.dateFromString(dateString as String)
}

func timeStringFromDate (date: NSDate) -> String {
    dateFormatter.dateFormat = "HH:mm"
    let locale = NSLocale (localeIdentifier: "en_US_POSIX")
    dateFormatter.locale = locale
    dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
    
    return dateFormatter.stringFromDate(date)
}

func rollingPercentMatched(currentCount : Int) ->Bool {
    return Double(currentCount) >= startTrigger
    
}
