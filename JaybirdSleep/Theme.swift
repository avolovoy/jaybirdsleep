//
//  Theme.swift
//  JaybirdSleep
//
//  Created by Alex Volovoy on 12/2/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import UIKit

class Theme: NSObject {
    
    static func backgroundColor() -> UIColor {
        return UIColor(red: 34/256, green: 34/256, blue: 34/256, alpha: 1)
    }
    static func tintColor() -> UIColor {
        return UIColor(red: 209/256, green: 235/256, blue: 35/256, alpha: 1)
    }
    static func fontColor() -> UIColor {
        return UIColor(red: 94/256, green: 94/256, blue: 94/256, alpha: 1)
    }

}
