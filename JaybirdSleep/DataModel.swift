//
//  DataModel.swift
//  JaybirdSleep
//
//  Created by Alex Volovoy on 12/2/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import Foundation

struct Epoch : CustomStringConvertible {
    let timestamp : String
    let activity_id  : Int
    
    init?(epochString: String) {
        let epochData = epochString.componentsSeparatedByString(",")
        if epochData.count < 2 {
            return nil
        }
        guard let activity = Int(epochData[0]) else {return nil}
        self.activity_id = activity
        self.timestamp = epochData[1]
        
    }
    
    var description: String {
        return "activity_id : \(activity_id) timestamp \(timestamp)"
    }
}

struct Period : CustomStringConvertible {
    var startTime : NSDate?
    var endTime : NSDate?
    var duration : NSTimeInterval?
    
    init() {
        self.startTime = nil
        self.endTime = nil
        self.duration = nil
    }
    
    func startTimeString () -> String {
        guard let date = startTime else { return "" }
        return timeStringFromDate(date)
    }
    func endTimeString () -> String {
        guard let date = endTime else { return "" }
        return timeStringFromDate(date)

    }
    func durationString () ->String {
        guard let duration = self.duration else { return "" }
        let hours = Int(duration/3600)
        let minutes = Int((duration / 60) % 60)
        return "\(hours):\(minutes)";
    }
    
    var description: String {
        return "\nSLEEP PERIOD\nStart time:\(startTime)\nEnd time:\(endTime)\nDuration:\(durationString()) - \(duration)) "
    }
}
