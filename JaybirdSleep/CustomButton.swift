//
//  CustomButton.swift
//  JaybirdSleep
//
//  Created by Alex Volovoy on 12/2/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    override init(frame: CGRect) {
        
        super.init(frame: frame)
        setupButton()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        setupButton()
    }
    
    func setupButton() {
        
        self.layer.cornerRadius = 3.0
        self.layer.borderWidth = 1.0
        self.layer.masksToBounds = true
        self.tintColor = Theme.tintColor()
    }
    
    override func tintColorDidChange() {
        super.tintColorDidChange()
        if (self.enabled) {
            self.layer.borderColor = Theme.tintColor().CGColor
        } else {
            self.layer.borderColor = UIColor.lightGrayColor().CGColor
        }
        
    }
    
    override func alignmentRectInsets() -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 5)
    }
    
    override var enabled: Bool {
        didSet {
            if (enabled) {
                self.layer.borderColor = Theme.tintColor().CGColor
            }
            else {
                self.layer.borderColor = UIColor.lightGrayColor().CGColor
            }
            
        }
    }


}
