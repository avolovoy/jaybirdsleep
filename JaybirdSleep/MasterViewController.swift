//
//  MasterViewController.swift
//  JaybirdSleep
//
//  Created by Alex Volovoy on 12/2/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var testFilesURLs = [NSURL]()


    override func viewDidLoad() {
        super.viewDidLoad()
        // Make Back button with no title
        self.tableView.backgroundColor = Theme.backgroundColor()
        self.tableView.tableFooterView = UIView(frame: CGRectZero)

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        // Do any additional setup after loading the view, typically from a nib.
        testFilesURLs = loadTestFiles()
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
    }

    override func viewWillAppear(animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let fileUrl = testFilesURLs[indexPath.row] 
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
                controller.localFileUrl = fileUrl
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Local project file(s) to test"
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return testFilesURLs.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)

        let fileUrl = testFilesURLs[indexPath.row]
        cell.textLabel!.text = fileUrl.pathComponents?.last
        cell.textLabel!.textColor = Theme.tintColor()
        return cell
    }

    // MARK Data
    
    func loadTestFiles()->[NSURL] {
        let mainBundle = NSBundle.mainBundle()
        if let files = mainBundle.URLsForResourcesWithExtension("csv", subdirectory: nil) {
            return files
        }
        return [NSURL]()
    }


}

