//
//  DetailViewController.swift
//  JaybirdSleep
//
//  Created by Alex Volovoy on 12/2/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet weak var runAnalyserButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var periodView: UIView!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!

    var localFileUrl: NSURL? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.localFileUrl {
            if let label = self.detailDescriptionLabel {
                self.title = detail.pathComponents?.last
                label.text = detail.pathComponents?.last
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = Theme.backgroundColor()
        self.periodView.hidden = true
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func runAnalyser(sender: AnyObject) {
        self.periodView.hidden = true 
        self.errorLabel.hidden = true
        if let fileURL = localFileUrl {
            do {
                let content = try String(contentsOfURL: fileURL, encoding: NSUTF8StringEncoding)
                analyseString(content)
            }  catch {
                print("Error : couldnt open file")
            }
            
        }
    }
    
    func analyseString (content: String) {
        do {
            if let period = try analyseDataString(content) {
                startTimeLabel.text = "Start Time : \(period.startTimeString())"
                endTimeLabel.text = "End Time : \(period.endTimeString())"
                durationLabel.text = "Duration : \(period.durationString())"
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.periodView.hidden = false
                })
                
                print(period)
            }
        }
        catch let error as DataError {
           displayErrorWithMessage(message: error.rawValue)
        }
        catch {
            displayErrorWithMessage(message: "Unknown error")
        }

    }
    
    func displayErrorWithMessage (message message : String ) {
        self.errorLabel.text = message
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.errorLabel.hidden = false
        })

    }


}

